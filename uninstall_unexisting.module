<?php

/**
 * @file
 */

/**
 * Implements hook_module_installed.
 *
 * @param array $modules
 *    Array of modules machine name that were installed.
 */
function uninstall_unexisting_modules_installed($modules) {
  // Set a new module as entry in the uninstall_unexisting table.
  foreach($modules as $module_name) {
    if ($module_name !== 'uninstall_unexisting') {
      /** @var Drupal\uninstall_unexisting\Service\UninstallUnexistingService $uu_service */
      $uu_service = \Drupal::service('uninstall_unexisting.service');
      $uu_service->setNewModule($module_name);
    }
  }
}

/**
 * Implements hook_module_preuninstall.
 *
 * @param $module_name
 *    The module machine name.
 */
function uninstall_unexisting_module_preuninstall($module_name) {
  // Recreate the module folder if it not exists before uninstallation.
  if ($module_name !== 'uninstall_unexisting') {
    /** @var Drupal\uninstall_unexisting\Service\UninstallUnexistingService $uu_service */
    $uu_service = \Drupal::service('uninstall_unexisting.service');
    if (!$uu_service->moduleFolderExists($module_name)) {
      $uu_service->recreateFolder($module_name);
    }
  }
}

/**
 * Implements hook_modules_uninstalled.
 *
 * @param array $modules
 *    Array of modules machine name that were uninstalled.
 * @param boolean $is_syncing
 *    TRUE if the module is being uninstalled as part of a configuration import.
 */
function uninstall_unexisting_modules_uninstalled($modules, $is_syncing) {
  // Remove the recreated module folders.
  foreach($modules as $module_name) {
    if ($module_name !== 'uninstall_unexisting') {
      /** @var Drupal\uninstall_unexisting\Service\UninstallUnexistingService $uu_service */
      $uu_service = \Drupal::service('uninstall_unexisting.service');
      $uu_service->cleanRecreated($module_name);
    }
  }
}
