<?php

namespace Drupal\uninstall_unexisting;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Modifies the module handler service.
 */
class UninstallUnexistingServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->hasDefinition('module_handler')) {
      $definition = $container->getDefinition('module_handler');
      $definition->setClass('Drupal\uninstall_unexisting\Service\ModuleHandlerOverride');
    }
  }
}
