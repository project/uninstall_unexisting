<?php

namespace Drupal\uninstall_unexisting\Service;

use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Repository for database-related helper methods.
 *
 * @ingroup uninstall_unexisting
 */
class UninstallUnexistingRepository implements UninstallUnexistingRepositoryInterface {

  use StringTranslationTrait;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;


  /**
   * The Drupal logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChanel;

  /**
   * Construct a repository object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation
   *   The translation service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The Logger factory service.
   */
  public function __construct(Connection $connection, TranslationInterface $translation, LoggerChannelFactoryInterface $logger) {
    $this->connection = $connection;
    $this->setStringTranslation($translation);
    $this->loggerChanel = $logger->get('uninstall_unexisting');
  }

  /**
   * Save an entry in the database.
   *
   * Exception handling is shown in this example. It could be simplified
   * without the try/catch blocks, but since an insert will throw an exception
   * and terminate your application if the exception is not handled, it is best
   * to employ try/catch.
   *
   * @param array $entry
   *   An array containing all the fields of the database record.
   *
   * @return int
   *   The number of updated rows.
   *
   * @throws \Exception
   *   When the database insert fails.
   */
  public function insert(array $entry) {
    $now = time();
    $entry['created'] = $now;
    $entry['changed'] = $now;
    if (!isset($entry['status'])) {
      $entry['status'] = self::STATUS_ENABLED;
    }

    try {
      $return_value = $this->connection->insert(self::TABLE_NAME)
        ->fields($entry)
        ->execute();
    }
    catch (\Exception $e) {
      $this->loggerChanel->warning($this->t('Insert failed. Message = %message', [
        '%message' => $e->getMessage(),
      ]));
    }
    return $return_value ?? NULL;
  }

  /**
   * Update an entry in the database.
   *
   * @param array $entry
   *   An array containing all the fields of the item to be updated.
   *
   * @return int
   *   The number of updated rows.
   */
  public function update(array $entry) {
    try {
      $count = $this->connection->update(self::TABLE_NAME)
        ->fields($entry)
        ->condition('mid', $entry['mid'])
        ->execute();
    }
    catch (\Exception $e) {
      $this->loggerChanel->warning($this->t('Update failed. Message = %message, query= %query', [
          '%message' => $e->getMessage(),
          '%query' => $e->query_string,
        ]
      ));
    }
    return $count ?? 0;
  }

  /**
   * Delete an entry from the database.
   *
   * @param array $entry
   *   An array containing at least the module identifier 'mid' element of the
   *   entry to delete.
   *
   * @see \Drupal\Core\Database\Connection::delete()
   */
  public function delete(array $entry) {
    $this->connection->delete(self::TABLE_NAME)
      ->condition('mid', $entry['mid'])
      ->execute();
  }

  /**
   * Read from the database using a filter array.
   *
   * @param array $entry
   *   An array containing all the fields used to search the entries in the
   *   table.
   *
   * @return object
   *   An object containing the loaded entries if found.
   *
   * @see \Drupal\Core\Database\Connection::select()
   */
  public function load(array $entry = []) {
    // Read all the fields from the dbtng_example table.
    $select = $this->connection
      ->select(self::TABLE_NAME)
      // Add all the fields into our select query.
      ->fields(self::TABLE_NAME);

    // Add each field and value as a condition to this query.
    foreach ($entry as $field => $value) {
      $select->condition($field, $value);
    }
    // Return the result in object format.
    return $select->execute()->fetchAllAssoc('mid', \PDO::FETCH_ASSOC);
  }

  /**
   * Load an entry by module name.
   *
   * @param $module_name
   *    Name of the module to load.
   *
   * @return boolean
   *    TRUE is exists, FALSE otherwise.
   */
  public function exists($module_name) {
    // Read all the fields from the dbtng_example table.
    $select = $this->connection
      ->select(self::TABLE_NAME)
      // Add all the fields into our select query.
      ->fields(self::TABLE_NAME);

    $select->condition('mid', $module_name);
      // Return the result in object format.
    return empty($select->execute()->fetchAll()) ? FALSE : TRUE;
  }

}
