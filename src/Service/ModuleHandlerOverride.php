<?php

namespace Drupal\uninstall_unexisting\Service;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandler;

class ModuleHandlerOverride extends ModuleHandler {

  /**
   * {@inheritdoc}
   */
  public function __construct($root, array $module_list, CacheBackendInterface $cache_backend) {
    // Bypass not existing module folders to avoid errors.
    foreach($module_list as $module_name => $module) {
      if (!file_exists($root.DIRECTORY_SEPARATOR.$module['pathname'])) {
        unset($module_list[$module_name]);
      }
    }
    parent::__construct($root, $module_list, $cache_backend);
  }
}
