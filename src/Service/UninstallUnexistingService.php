<?php

namespace Drupal\uninstall_unexisting\Service;

use Drupal\Core\Extension\Exception\UnknownExtensionException;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\Extension;
use Drupal\Core\File\FileSystemInterface;

class UninstallUnexistingService implements UninstallUnexistingInterface {

  /**
   * The Drupal module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The UninstallUnexistingRepository service.
   *
   * @var UninstallUnexistingRepository
   */
  protected $uninstallUnexistingRepository;

  /**
   * The Drupal ModuleExtensionList service.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $extensionListModule;

  /**
  * UninstallUnexistingService constructor.
  *
  * @param \Drupal\Core\Extension\ModuleHandlerInterface $handler
  */
  public function __construct(ModuleHandlerInterface $handler,
                              UninstallUnexistingRepository $uu_repository,
                              ModuleExtensionList $extension_list_module
    ) {
    $this->moduleHandler = $handler;
    $this->uninstallUnexistingRepository = $uu_repository;
    $this->extensionListModule = $extension_list_module;
  }

  /**
   * {@inheritdoc}
   */
  public function getEnabledModules(): array {
    $modules_enabled = [];
    try {
      $modules = $this->moduleHandler->getModuleList();
    }
    catch(\Exception $e) {
      $message = sprintf('Uninstall unexisting module cannot load a module list: %s', $e->getMessage());
      \Drupal::logger('uninstall_unexisting')->error($message);
      echo $message.PHP_EOL;
    }
    foreach($modules as $module_name => $module) {
      if (substr($module->getPath(), 0, 5) !== 'core/') {
        $modules_enabled[$module_name] = $module;
      }
    }
    return $modules_enabled;
  }

  /**
   * {@inheritdoc}
   */
  public function moduleFolderExists($module_name): bool {
    $module_folder_exists = FALSE;
    try {
      $module = $this->moduleHandler->getModule($module_name);
      $module_info_file = \Drupal::root().DIRECTORY_SEPARATOR.$module->getPathname();
      if (file_exists($module_info_file)) {
        $module_folder_exists = TRUE;
      }
    }
    catch(UnknownExtensionException $e) {
      $module_folder_exists = FALSE;
    }
    return $module_folder_exists;
  }

  /**
   * {@inheritdoc}
   */
  public function recreateFolder($module_name) {
    $entry['mid'] = $module_name;
    $record = $this->uninstallUnexistingRepository->load($entry);
    if (isset($record[$module_name])) {
      $entry = $record[$module_name];
    }

    // Module directory path to recreate.
    $module_path = \Drupal::root().DIRECTORY_SEPARATOR.$entry['path'];

    // Recreating module folder.
    if (!file_exists($module_path)) {
      /** @var FileSystemInterface $file_system */
      $file_system = \Drupal::service('file_system');
      $file_system->mkdir($module_path);
      $file_system->saveData($entry['info'], $module_path.DIRECTORY_SEPARATOR.$module_name.'.info.yml');
      $file_system->saveData($entry['install'], $module_path.DIRECTORY_SEPARATOR.$module_name.'.install');
    }

    // Update the module entry.
    $update_entry['mid'] = $entry['mid'];
    $update_entry['status'] = UninstallUnexistingRepository::STATUS_RECREATED;
    $this->uninstallUnexistingRepository->update($update_entry);
  }

  /**
   * {@inheritdoc}
   */
  public function cleanRecreated($module_name) {
    $record = $this->uninstallUnexistingRepository->load(['mid' => $module_name]);
    if (isset($record[$module_name])) {
      $entry = $record[$module_name];
    }
    if (!empty($entry) && $entry['status'] === UninstallUnexistingRepository::STATUS_RECREATED) {
      // The module directory path recreated.
      $module_path = \Drupal::root().DIRECTORY_SEPARATOR.$entry['path'];
      if (file_exists($module_path)) {
        // Remove recreated module folder.
        /** @var FileSystemInterface $file_system */
        $file_system = \Drupal::service('file_system');
        $file_system->deleteRecursive($module_path);
      }

      // Update the module entry.
      $update_entry['mid'] = $entry['mid'];
      $update_entry['status'] = UninstallUnexistingRepository::STATUS_DISABLED;
      $this->uninstallUnexistingRepository->update($update_entry);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setModule($machine_name, Extension $module, $status = NULL) {
    // Get content of the module .info.yml file.
    $info_file_path = \Drupal::root().DIRECTORY_SEPARATOR.$module->getPathname();
    $info_file_content = file_get_contents($info_file_path);

    // Get content of the module .install file if exists.
    $install_file_content = '';
    $install_file_path = str_replace( '.info.yml', '.install', $info_file_path);
    if (file_exists($install_file_path)) {
      $install_file_content = file_get_contents($install_file_path);;
    }

    $info = $this->extensionListModule->getExtensionInfo($machine_name);
    $entry = [
      'mid' => $machine_name,
      'version' => $info['version'],
      'path' => dirname($module->getPathname()),
      'info' => $info_file_content,
      'install' => $install_file_content,
    ];
    if ($status) {
      $entry['status'] = $status;
    }

    if ($this->uninstallUnexistingRepository->exists($machine_name)) {
      $this->uninstallUnexistingRepository->update($entry);
    }
    else {
      $this->uninstallUnexistingRepository->insert($entry);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setNewModule($machine_name) {
    $module = $this->moduleHandler->getModule($machine_name);
    $this->setModule($machine_name, $module, UninstallUnexistingRepository::STATUS_ENABLED);
  }
}
