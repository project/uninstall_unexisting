<?php

namespace Drupal\uninstall_unexisting\Service;

use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Repository interface for database-related helper methods.
 *
 * @ingroup uninstall_unexisting
 */

namespace Drupal\uninstall_unexisting\Service;

/**
 * Repository interface for database-related helper methods.
 *
 * @ingroup uninstall_unexisting
 */
interface UninstallUnexistingRepositoryInterface {
  // Table to store modules information
  const TABLE_NAME = 'uninstall_unexisting';

  // Available status of modules
//  const STATUS_TO_ENABLE = 'to_enable';
  const STATUS_ENABLED = 'enabled';
  const STATUS_RECREATED = 'recreated';
  const STATUS_DISABLED = 'disabled';


  /**
   * Save an entry in the database.
   *
   * Exception handling is shown in this example. It could be simplified
   * without the try/catch blocks, but since an insert will throw an exception
   * and terminate your application if the exception is not handled, it is best
   * to employ try/catch.
   *
   * @param array $entry
   *   An array containing all the fields of the database record.
   *
   * @return int
   *   The number of updated rows.
   *
   * @throws \Exception
   *   When the database insert fails.
   */
  public function insert(array $entry);

  /**
   * Update an entry in the database.
   *
   * @param array $entry
   *   An array containing all the fields of the item to be updated.
   *
   * @return int
   *   The number of updated rows.
   */
  public function update(array $entry);

  /**
   * Delete an entry from the database.
   *
   * @param array $entry
   *   An array containing at least the module identifier 'mid' element of the
   *   entry to delete.
   *
   * @see \Drupal\Core\Database\Connection::delete()
   */
  public function delete(array $entry);

  /**
   * Read from the database using a filter array.
   *
   * @param array $entry
   *   An array containing all the fields used to search the entries in the
   *   table.
   *
   * @return object
   *   An object containing the loaded entries if found.
   *
   * @see \Drupal\Core\Database\Connection::select()
   */
  public function load(array $entry = []);

  /**
   * Load an entry by module name.
   *
   * @param $module_name
   *    Name of the module to load.
   *
   * @return mixed
   */
  public function exists($module_name);

}
