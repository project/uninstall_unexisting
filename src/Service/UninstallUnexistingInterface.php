<?php

namespace Drupal\uninstall_unexisting\Service;

use Drupal\Core\Extension\Extension;

interface UninstallUnexistingInterface {

  /**
   * Get the list of enabled modules, without core modules.
   *
   * @return array
   *    Array of enabled modules (Drupal\Core\Extension\Extension)
   */
  public function getEnabledModules(): array;

  /**
   * Check if the module folder exists on the file system.
   *
   * @param $module_name
   *    Machine name of a module.
   *
   * @return bool
   *    TRUE if the folder exists, FALSE otherwise.
   */
  public function moduleFolderExists($module_name): bool;

  /**
   * Create a module folder, .info.yml and .install files for an existing module in the uninstall_unexisting table.
   *
   * @param $module_name
   *    The module name to recreate.
   *
   * @return void
   */
  public function recreateFolder($module_name);

  /**
   * Remove the module folder if it was recreated by uninstall_unexisting.
   *
   * @param string $module_name
   *    A module name.
   *
   * @return void
   */
  public function cleanRecreated($module_name);

  /**
   * Create a module line in the uninstall_unexisting table.
   *
   * @param string $machine_name
   *    Machine name of a module.
   * @param \Drupal\Core\Extension\Extension $module
   *    The Drupal module Extension object.
   * @param null $status
   *    The optional status to set.
   *
   * @return void
   */
  public function setModule($machine_name, Extension $module, $status = NULL);

  /**
   * Create a module line for module enabled in the uninstall_unexisting table.
   *
   * @param $machine_name
   *    The module machine name.
   * @return void
   */
  public function setNewModule($machine_name);
}
